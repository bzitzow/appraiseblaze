<?php
namespace AppraiseBlaze\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilder;

class PropertyFormType extends AbstractType
{
  function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('photo', 'file', array('required' => false));
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'AppraiseBlaze\MainBundle\Entity\PropertyFormType',
    ));
  }

  public function getName()
  {
    return('appraiseblaze_main_property');
  }
}
