<?php

namespace AppraiseBlaze\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('addressOfProperty')
            ->add('dateOfInspection')
            ->add('yearBuilt')
            ->add('squareFootage')
            ->add('landArea')
            ->add('rentableBuildingArea')
            ->add('ownerOfRecord')
            ->add('unitMix')
            ->add('licenseNumber')
            ->add('extentOfInspection')
            ->add('highestAndBestUse')
            ->add('comments')
            ->add('propertyType')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppraiseBlaze\MainBundle\Entity\Report',
            'csrf_protection' => false
        ));
    }

    public function getName()
    {
        return 'appraiseblaze_mainbundle_demotype';
    }
}
