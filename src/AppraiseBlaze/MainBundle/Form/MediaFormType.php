<?php
namespace AppraiseBlaze\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilder;

class MediaFormType extends AbstractType
{
    function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', 'file', array('required' => true))
            ->add('demo_id')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppraiseBlaze\MainBundle\Entity\Media',
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return('appraiseblaze_main_media');
    }
}
