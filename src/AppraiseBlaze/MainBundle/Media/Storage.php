<?php

namespace AppraiseBlaze\MainBundle\Media;

use AppraiseBlaze\MainBundle\Entity\Media;
use Symfony\Component\HttpFoundation\File\File;

class Storage
{
    private $directory;

    public function __construct($directory)
    {
        $this->directory = $directory;
    }

    public function getDirectory()
    {
        return $this->directory;
    }

    public function store(Media $media)
    {
        $file = $media->getFile();

        if (null === $file) {
            throw new \RuntimeException('Cannot store a media having no file.');
        }

        // loop just in case the generated filename matches an existing file
        do {
            $media->setFilename($this->generateFilename($file));
            $path = $this->getPath($media);
        } while (file_exists($path));

        $file = $file->move(dirname($path), basename($path));

        $media->setFile($file);
        $media->setSize($file->getSize());
        $media->setMimeType($file->getMimeType());
        $media->setChecksum(md5_file($path));
    }

    public function remove($media)
    {
        if (null === $media->getFilename()) {
            return;
        }

        $path = $this->getPath($media);

        if (file_exists($path)) {
            unlink($path);
        }
    }

    public function getPath(Media $media)
    {
        return $this->directory.'/'.$media->getFilename();
    }

    private function generateFilename(File $file)
    {
        $hash = sha1(uniqid(mt_rand(), true));

        return sprintf(
            '%s/%s/%s.%s',
            substr($hash, 0, 3),
            substr($hash, 3, 3),
            substr($hash, 6, 3),
            $file->guessExtension()
        );
    }
}
