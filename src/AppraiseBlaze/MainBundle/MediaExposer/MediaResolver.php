<?php
namespace AppraiseBlaze\MainBundle\MediaExposer;

use AppraiseBlaze\MainBundle\Entity\Media;
use MediaExposer\PathResolver;
use MediaExposer\SourceResolver;

class MediaResolver implements PathResolver, SourceResolver
{
    private $directory;
    private $webDirectory = '/media';

//    public function __construct($directory, $webDirectory)
//    {
//        $this->directory     = $directory;
//        $this->webDirectory  = $webDirectory;
//    }

    /**
     * {@inheritDoc}
     */
    public function supports($media, array $options)
    {
        return $media instanceof Media;
    }

    /**
     * {@inheritDoc}
     */
    public function getPath($media, array $options)
    {
        if (null === $media->getFilename()) {
            return;
        }

        return sprintf('%s/%s', $this->directory, $media->getFilename());
    }

    /**
     * {@inheritDoc}
     */
    public function getSource($media, array $options = array())
    {
        if (null === $media->getFilename()) {
            return;
        }

        $source = sprintf('%s/%s', $this->webDirectory, $media->getFilename());

        if (empty($options['filter'])) {
            return $source;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getSourceType($media, array $options)
    {
        return SourceResolver::TYPE_RELATIVE;
    }
}
