<?php

namespace AppraiseBlaze\MainBundle\Controller;

use AppraiseBlaze\MainBundle\Entity\Report;
use AppraiseBlaze\MainBundle\Entity\Media;
use AppraiseBlaze\MainBundle\Form\ReportType;
use AppraiseBlaze\MainBundle\Form\MediaFormType;
use AppraiseBlaze\MainBundle\Form\PropertyFormType;
use AppraiseBlaze\Mainbundle\Entity\Property;
use Doctrine\Common\Util\Debug;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $reportRepo = $this->getReportRepository();
        $reports    = $reportRepo->findAll();
        $data       = array('reports' => $reports);
        return $this->render('AppraiseBlazeMainBundle:Default:index.html.twig', $data);
    }

    public function getAppraisalPdfByIdAction(Request $request)
    {
        $id = $request->query->get('demo_id');
        $demoRepository = $this->getReportRepository();
        $demo = $demoRepository->findOneById($id);

        $PDF_DIR = __DIR__.'/../lib/pdf';
        include($PDF_DIR.'/fpdf17/fpdf.php');
        include($PDF_DIR.'/fpdi/fpdi.php');

        $errors = array();
        $data = array(
//            'addressOfProperty' => '1715 I st, Sacramento, CA 95814',
            'addressOfProperty' => $demo->getAddressOfProperty(),
            'dateOfInspection' => $demo->getDateOfInspection(),
            'yearBuilt' => $demo->getYearBuilt(),
            'propertyType' => $demo->getPropertyType(),
            'squareFootage' => $demo->getSquareFootage(),
            'landArea' => $demo->getLandArea(),
            'rentableBuildingArea' => $demo->getRentableBuildingArea(),
            'ownerOfRecord' => $demo->getOwnerOfRecord(),
            'unitMix' => $demo->getUnitMix(),
            'licenseNumber' => $demo->getLicenseNumber(),
//            'extentOfInspection' => $demo->getExtentOfInspection(),
            'extentOfInspection' => ' ',
            'highestAndBestUse' => $demo->getHighestAndBestUse(),
            'comments' => $demo->getComments(),

            'image1' => $PDF_DIR.'/src/appraisal1.jpg',
            'image2' => $PDF_DIR.'/src/appraisal_floor1.jpg',
        );

        $img1 = $PDF_DIR.'/src/appraisal1.jpg';
        $img2 = $PDF_DIR.'/src/appraisal_floor1.jpg';

        $pdf = new \FPDI('P', 'mm', 'letter');
        $pagecount = $pdf->setSourceFile($PDF_DIR.'/src/appraiseblaze.pdf');
        $tplidx = $pdf->importPage(1, '/MediaBox');

        $pdf->AddPage('P');
        $pdf->useTemplate($tplidx);
        $pdf->SetFont('arial', '', 15);

        $pdf->SetXY(126, 12);
        $pdf->multiCell(0, 8, $data['addressOfProperty']);

        $pdf->SetXY(126, 19);
        $pdf->multiCell(0, 8, number_format($data['squareFootage'], 0, '.', ',').' sq. ft.', 0, 'R');

        $pdf->SetXY(63, 137);
        $pdf->multiCell(0, 8, $data['dateOfInspection'], 0, 10);

        $pdf->SetXY(63, 144);
        $pdf->multiCell(0, 8, $data['propertyType']);

        $pdf->SetXY(63, 151);
        $pdf->multiCell(0, 8, $data['yearBuilt']);

        $pdf->SetXY(63, 158);
        $pdf->multiCell(0, 8, $data['squareFootage']);

        $pdf->SetXY(63, 165);
        $pdf->multiCell(0, 8, $data['landArea']);

        $pdf->SetXY(63, 172);
        $pdf->multiCell(0, 8, $data['rentableBuildingArea']);

        $pdf->SetXY(63, 179);
        $pdf->multiCell(0, 8, $data['comments']);

        $pdf->SetXY(174, 137);
        $pdf->multiCell(0, 8, $data['ownerOfRecord']);

        $pdf->SetXY(174, 144);
        $pdf->multiCell(0, 8, $data['unitMix']);

        $pdf->SetXY(174, 151);
        $pdf->multiCell(0, 8, substr($data['extentOfInspection'], 0, 10));

        $pdf->SetXY(174, 158);
        $pdf->multiCell(0, 8, substr($data['highestAndBestUse'], 0, 10));

        $pdf->SetXY(174, 166);
        $pdf->multiCell(0, 8, substr($data['licenseNumber'], 0, 10));

        $pdf->Image($img1, 63, 36, 140, 80);
        $pdf->Image($img2, 63, 196, 140, 70);

        return new Response($pdf->Output('appraisal.pdf', 'D'), 200);
    }

    public function homeAction()
    {
        return new Response('home', 200);
    }
    public function helloAction($name)
    {
        return $this->render('AppraiseBlazeMainBundle:Default:index.html.twig', array('name' => $name));
    }

    public function postProperty(Request $request)
    {
        $form = $this->createForm(new PropertyFormType(), new Property());
    }

    public function mediaAction(Request $request)
    {
        $form = $this->createForm(new MediaFormType(), new Media());
        $form->bind($request);

        if ($form->isValid()) {
            $media = $form->getData();
            $entityManager = $this->get('doctrine')->getManager();
            $entityManager->persist($media);
            $entityManager->flush();
            $resolver = $this->container->get('appraise_blaze.media_resolver');
            $data = array(
                'id' => $media->getId(),
                'source' => $resolver->getSource($media),
            );
        } else {
            $messages = array();
            if ($errors = $form->getErrors()) {
                foreach ($errors as $error) {
                    $messages[] = array('message' => $error->getMessage(), 'parameters' => $error->getMessageParameters());
                }
            } else {
                $messages[] = array('message' => 'Invalid request. Unknown error.');
            }

            $data = array('error' => $messages);
        }

        return new JsonResponse($data, 200);
    }

    public function showMediaAction(Request $request)
    {
        $em  = $this->get('doctrine')->getManager();
        $mediaRepo      = $em->getRepository('AppraiseBlazeMainBundle:Media');
        $medias = $mediaRepo->findAll();
        $data['medias'] = $medias;
        return $this->render('AppraiseBlazeMainBundle:Default:showMedia.html.twig', $data);
    }

    public function getClientPropertyAddressAction()
    {
        return new JsonResponse('1234 Address St. Sacramento, CA 95678', 200);
    }

    public function postDemoFormAction(Request $request)
    {
        $form = $this->createForm(new ReportType(), new Report());
        $form->bind($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $manager = $this->get('doctrine')->getManager();
            $manager->persist($data);
            $manager->flush();

            $data = array('id' => $data->getId());
        } else {
            $messages = array();
            if ($errors = $form->getErrors()) {
                foreach ($errors as $error) {
                    $messages[] = array('message' => $error->getMessage(), 'parameters' => $error->getMessageParameters());
                }
            } else {
                $messages[] = array('message' => 'Invalid request. Unknown error.');
            }

            $data = array('error' => $messages);
        }

        return new JsonResponse($data, 200);
    }

    public function getReportRepository()
    {
        return $this->get('doctrine')->getManager()->getRepository('AppraiseBlazeMainBundle:Report');
    }
}

