<?php

namespace AppraiseBlaze\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Report
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Report
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="addressOfProperty", type="string", length=255, nullable=true)
     */
    private $addressOfProperty;

    /**
     * @var string
     *
     * @ORM\Column(name="dateOfInspection", type="string", length=255, nullable=true)
     */
    private $dateOfInspection;

    /**
     * @var string
     *
     * @ORM\Column(name="yearBuilt", type="string", length=255, nullable=true)
     */
    private $yearBuilt;

    /**
     * @var string
     *
     * @ORM\Column(name="squareFootage", type="string", length=255, nullable=true)
     */
    private $squareFootage;

    /**
     * @var string
     *
     * @ORM\Column(name="landArea", type="string", length=255, nullable=true)
     */
    private $landArea;

    /**
     * @var string
     *
     * @ORM\Column(name="rentableBuildingArea", type="string", length=255, nullable=true)
     */
    private $rentableBuildingArea;

    /**
     * @var string
     *
     * @ORM\Column(name="ownerOfRecord", type="string", length=255, nullable=true)
     */
    private $ownerOfRecord;

    /**
     * @var string
     *
     * @ORM\Column(name="unitMix", type="string", length=255, nullable=true)
     */
    private $unitMix;

    /**
     * @var string
     *
     * @ORM\Column(name="licenseNumber", type="string", length=255, nullable=true)
     */
    private $licenseNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="extentOfInspection", type="string", nullable=true)
     */
    private $extentOfInspection;

    /**
     * @var string
     *
     * @ORM\Column(name="highestAndBestUse", type="string", length=255, nullable=true)
     */
    private $highestAndBestUse;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="string", length=255, nullable=true)
     */
    private $comments;

    /**
     * @var string
     *
     * @ORM\Column(name="propertyType", type="string", length=255, nullable=true)
     */
    private $propertyType;

    /**
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;

    public function __construct()
    {
        $this->name = "Report_Name_Here";
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set addressOfProperty
     *
     * @param string $addressOfProperty
     * @return Demo
     */
    public function setAddressOfProperty($addressOfProperty)
    {
        $this->addressOfProperty = $addressOfProperty;

        return $this;
    }

    /**
     * Get addressOfProperty
     *
     * @return string
     */
    public function getAddressOfProperty()
    {
        return $this->addressOfProperty;
    }

    /**
     * Set dateOfInspection
     *
     * @param string $dateOfInspection
     * @return Demo
     */
    public function setDateOfInspection($dateOfInspection)
    {
        $this->dateOfInspection = $dateOfInspection;

        return $this;
    }

    /**
     * Get dateOfInspection
     *
     * @return string
     */
    public function getDateOfInspection()
    {
        return $this->dateOfInspection;
    }

    /**
     * Set yearBuilt
     *
     * @param string $yearBuilt
     * @return Demo
     */
    public function setYearBuilt($yearBuilt)
    {
        $this->yearBuilt = $yearBuilt;

        return $this;
    }

    /**
     * Get yearBuilt
     *
     * @return string
     */
    public function getYearBuilt()
    {
        return $this->yearBuilt;
    }

    /**
     * Set squareFootage
     *
     * @param string $squareFootage
     * @return Demo
     */
    public function setSquareFootage($squareFootage)
    {
        $this->squareFootage = $squareFootage;

        return $this;
    }

    /**
     * Get squareFootage
     *
     * @return string
     */
    public function getSquareFootage()
    {
        return $this->squareFootage;
    }

    /**
     * Set landArea
     *
     * @param string $landArea
     * @return Demo
     */
    public function setLandArea($landArea)
    {
        $this->landArea = $landArea;

        return $this;
    }

    /**
     * Get landArea
     *
     * @return string
     */
    public function getLandArea()
    {
        return $this->landArea;
    }

    /**
     * Set rentableBuildingArea
     *
     * @param string $rentableBuildingArea
     * @return Demo
     */
    public function setRentableBuildingArea($rentableBuildingArea)
    {
        $this->rentableBuildingArea = $rentableBuildingArea;

        return $this;
    }

    /**
     * Get rentableBuildingArea
     *
     * @return string
     */
    public function getRentableBuildingArea()
    {
        return $this->rentableBuildingArea;
    }

    /**
     * Set ownerOfRecord
     *
     * @param string $ownerOfRecord
     * @return Demo
     */
    public function setOwnerOfRecord($ownerOfRecord)
    {
        $this->ownerOfRecord = $ownerOfRecord;

        return $this;
    }

    /**
     * Get ownerOfRecord
     *
     * @return string
     */
    public function getOwnerOfRecord()
    {
        return $this->ownerOfRecord;
    }

    /**
     * Set unitMix
     *
     * @param string $unitMix
     * @return Demo
     */
    public function setUnitMix($unitMix)
    {
        $this->unitMix = $unitMix;

        return $this;
    }

    /**
     * Get unitMix
     *
     * @return string
     */
    public function getUnitMix()
    {
        return $this->unitMix;
    }

    /**
     * Set licenseNumber
     *
     * @param string $licenseNumber
     * @return Demo
     */
    public function setLicenseNumber($licenseNumber)
    {
        $this->licenseNumber = $licenseNumber;

        return $this;
    }

    /**
     * Get licenseNumber
     *
     * @return string
     */
    public function getLicenseNumber()
    {
        return $this->licenseNumber;
    }

    /**
     * Set extentOfInspection
     *
     * @param string $extentOfInspection
     * @return Demo
     */
    public function setExtentOfInspection($extentOfInspection)
    {
        $this->extentOfInspection = $extentOfInspection;

        return $this;
    }

    /**
     * Get extentOfInspection
     *
     * @return string
     */
    public function getExtentOfInspection()
    {
        return $this->extentOfInspection;
    }

    /**
     * Set highestAndBestUse
     *
     * @param string $highestAndBestUse
     * @return Demo
     */
    public function setHighestAndBestUse($highestAndBestUse)
    {
        $this->highestAndBestUse = $highestAndBestUse;

        return $this;
    }

    /**
     * Get highestAndBestUse
     *
     * @return string
     */
    public function getHighestAndBestUse()
    {
        return $this->highestAndBestUse;
    }

    /**
     * @param string $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param string $propertyType
     */
    public function setPropertyType($propertyType)
    {
        $this->propertyType = $propertyType;
    }

    /**
     * @return string
     */
    public function getPropertyType()
    {
        return $this->propertyType;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

}
