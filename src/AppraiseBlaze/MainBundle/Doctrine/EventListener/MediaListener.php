<?php
namespace AppraiseBlaze\MainBundle\Doctrine\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\HttpFoundation\File\File;
use AppraiseBlaze\MainBundle\Entity\Media;
use AppraiseBlaze\MainBundle\Media\Storage;

class MediaListener implements EventSubscriber
{
    private $storage;

    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * {@inheritDoc}
     */
    public function getSubscribedEvents()
    {
        return array(
            Events::prePersist,
            Events::postRemove,
            Events::postLoad,
        );
    }

    public function prePersist(LifecycleEventArgs $event)
    {
        if (!$this->supports($event)) {
            return;
        }

        $this->storage->store($event->getEntity());
    }

    public function postRemove(LifecycleEventArgs $event)
    {
        if (!$this->supports($event)) {
            return;
        }

        $this->storage->remove($event->getEntity());
    }

    public function postLoad(LifecycleEventArgs $event)
    {
        if (!$this->supports($event)) {
            return;
        }

        $media = $event->getEntity();
        $media->setFile(new File($this->storage->getPath($media), false));
    }

    private function supports(LifecycleEventArgs $event)
    {
        if (!$event->getEntity() instanceof Media) {
            return false;
        }

        return true;
    }
}
