  function editboard_up_auth ()
  {
    update_form(array('board_up_id','board_up_location','board_up_fire','board_up_water','board_up_vehicle','board_up_police','board_up_code','board_up_contact_time','board_up_contact_date','board_up_contact_day','board_up_time_of_arrival','board_up_release_time','board_up_emerg_sig_id','board_up_property_rep_sig_id','board_up_property_rep_phone','board_up_owner_name','board_up_owner_phone','board_up_owner_alt_phone','board_up_owner_address','board_up_insurance_co','board_up_insurance_phone','board_up_insurance_agent','board_up_insurance_agent_phone','board_up_sheets','board_up_locks','board_up_hasps','board_up_chain','board_up_two_by_four','board_up_tarp','board_up_bats','board_up_fence','board_up_men','board_up_bolts','board_up_shoring','board_up_other'), 'board_up_auth', 'board_up_id');
    return '{"results":"updated"}';
  }

  function editchange_orders ()
  {
    update_form(array('change_order_id','change_order_project_id','change_order_owner','change_order_location','change_order_phone','change_order_alt_phone','change_order_desc','change_order_rbi_sig_id','change_order_owner_sig_id','change_order_date'), 'change_orders', 'change_order_id');
    return '{"results":"updated"}';
  }

  function editcontacts ()
  {
    update_form(array('contact_id','contact_first','contact_last','contact_project_id','contact_email'), 'contacts', 'contact_id');
    return '{"results":"updated"}';
  }

  function editcontents_releases ()
  {
    update_form(array('content_release_id','content_release_project_id','content_release_owner','content_release_location','content_release_home_phone','content_release_alt_phone','content_release_date','content_release_owner_sig_id','content_release_rbi_sig_id'), 'contents_releases', 'content_release_id');
    return '{"results":"updated"}';
  }

  function editdraw_schedual_a ()
  {
    update_form(array('draw_schedual_id','draw_schedual_project_id','draw_schedual_owner_name','draw_schedual_location','draw_schedual_phone','draw_schedual_alt_phone','draw_schedual_start_balance','draw_schedual_mortgage_release','draw_schedual_half_release','draw_schedual_ninety_release','draw_schedual_supp_funds','draw_schedual_balance_retention','draw_schedual_rib_sig_id','draw_schedual_owner_sig_id','draw_schedual_date'), 'draw_schedual_a', 'draw_schedual_id');
    return '{"results":"updated"}';
  }

  function editemergency_service_auth ()
  {
    update_form(array('emergency_service_auth_id','emergency_service_auth_project_id','emergency_service_auth_owner','emergency_service_auth_location','emergency_service_auth_phone','emergency_service_auth_alt_phone','emergency_service_auth_plumbing','emergency_service_auth_gas','emergency_service_auth_power','emergency_service_auth_electrical','emergency_service_auth_roof','emergency_service_auth_dry','emergency_service_auth_wet','emergency_service_auth_other','emergency_service_auth_desc','emergency_service_auth_rbi_sig_id','emergency_service_auth_owner_sig_id','emergency_service_auth_date'), 'emergency_service_auth', 'emergency_service_auth_id');
    return '{"results":"updated"}';
  }

  function editmortgage_authorizations ()
  {
    update_form(array('mortgage_authorization_id','mortgage_authorization_project_id','mortgage_authorization_owner_first','mortgage_authorization_owner_last','mortgage_authorization_owner_phone','mortgage_authorization_company_name','mortgage_authorization_company_phone','mortgage_authorization_company_address','mortgage_authorization_loan_number','mortgage_authorization_ss','mortgage_authorization_property_address','mortgage_authorization_owner_sig_id','mortgage_authorization_date'), 'mortgage_authorizations', 'mortgage_authorization_id');
    return '{"results":"updated"}';
  }

  function editright_to_cancel ()
  {
    update_form(array('right_to_cancel_id','right_to_cancel_project_id','right_to_cancel_desc','right_to_cancel_owner_sig_id','right_to_cancel_owner2_sig_id','right_to_cancel_date'), 'right_to_cancel', 'right_to_cancel_id');
    return '{"results":"updated"}';
  }

