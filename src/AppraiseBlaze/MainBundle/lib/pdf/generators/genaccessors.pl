#!/usr/bin/perl

sub sanitize
{
  my @fields = @_;
  my ($field, $sanitized);
  foreach $field (@fields)
  {
    $sanitized .= "    \$$field = mysql_escape_string(\$$field);\n";
  }
  $sanitized;
}

sub genedit
{
  my $table = shift;
  shift;
  my @fields = @_;
  my ($field, $updates, @updates, $parameters, @parameters, @data);
  # "UPDATE $table set ".implode(',',$qsl1)." WHERE $key='".$data[$key]."'";
  
  $key = $fields[0];
  foreach $field (@fields)
  {
    if ($field ne $key)
    {	push @updates, "'$field'";	}
    push @parameters, "\$$field";
    push @data, '"'.$field.'"=>$'.$field;
  }
  $updates = join (', ', @updates);
  $parameters = join (', ', @parameters);
  $data = join(',', @data);
  #my $sanitize = &sanitize(@fields);
  
  print <<EDIT
  function edit$table ($parameters)
  {
    update_form(array($updates));
    return "{\"results\":[{\"success\":\"$table updated\"}]}";
  }

EDIT
;
}

sub genedit2
{
  my $table = shift;
  my @fields = @_;
  my ($field, $parameters, @parameters, $data, @p2);
  # "UPDATE $table set ".implode(',',$qsl1)." WHERE $key='".$data[$key]."'";
  
  $key = $fields[0];
  foreach $field (@fields)
  {
    push @parameters, "\$$field";
    push @p2, "'$field'";
  }
  $parameters = join (', ', @parameters);
  $data = join(',', @p2);
  #my $sanitize = &sanitize(@fields);
  
  print <<EDIT
  function edit$table ()
  {
    update_form(array($data), '$table', '$key');
    return '{"results":"updated"}';
  }

EDIT
;
}

sub genadd
{
  my $table = shift;
  #shift;
  my @fields = @_;
  my ($field, $fields, $values, @values, $parameters, @parameters, @data);
  # "UPDATE $table set ".implode(',',$qsl1)." WHERE $key='".$data[$key]."'";
  
  $key = shift @fields;
  foreach $field (@fields)
  {
    push @values, "'\$$field'";
    push @parameters, "\$$field";
    push @data, '"'.$field.'"=>$'.$field;
  }
  $parameters = join (', ', @parameters);
  $data = join(',', @data);
  $fields = join (', ', @fields);
  $values = join (', ', @values);
  my $sanitize = &sanitize(@fields);
    
  print <<EDIT
  function add$table ($parameters)
  {
    mysql_query("INSERT INTO $table ($fields) VALUES ($values)");
    \$id = mysql_insert_id();
    \$data = array($data, '$key'=>\$id);
    return '{"results":'.json_encode(\$data).'}';
  }

EDIT
;

}

sub genget
{
  my $table = shift;
  shift;
  my @fields = @_;
  my ($field, $parameters, @parameters);
  # "SELECT ... FROM $table WHERE $key='".$data[$key]."'";
  
  $key = $fields[0];
  foreach $field (@fields)
  {
    if ($field ne $key)
    {	push @updates, "$field = '\$$field'";	}
    push @parameters, "\$$field";
  }
  $parameters = join (', ', @parameters);
  $fields = join (', ', @fields);
  my $sanitize = &sanitize(@fields);
  
  print <<EDIT
  function get$table ($parameters)
  {
    \$result = mysql_query("SELECT $fields FROM $table WHERE $key='".\$$key."'");
    \$ress = array();
    while (\$res = mysql_fetch_assoc(\$result))
    {
      \$ress[] = \$res;
    }
    echo '{"results":'.json_encode(\$ress).'}';
  }

EDIT
;
}

sub genAPI
{
# ## Create User
# http://rbi.appmatrix.mobi/api/rbi_api.php?q=createuser&firstname=testers&lastname=tester&type=0&email=test@appmatrixinc.com&password=12345
# `{"results":[{"success":"user created","user_id":"2","user_first":"testers","user_last":"tester"}]}`
  my $table = shift;
  my $uctable = ucfirst($table);
  shift;
  my @fields = @_;
  my ($field, $parameters, @parameters, @required);
  # "SELECT ... FROM $table WHERE $key='".$data[$key]."'";
  
  $key = $fields[0];
  foreach $field (@fields)
  {
    if ($field ne $key)
    {	#push @updates, "$field = '\$$field'";
    }
    push @parameters, "\$$field";
    push @required, "isset (\$$field)";
  }
  $parameters = join (', ', @parameters);
  $required = join(' && ', @required);
  $fields = join (', ', @fields);
  my $sanitize = &sanitize(@fields);
  
  print <<EDIT
  else if(strcasecmp(\$q,"edit$table") == 0){
	echo "{\\"results\\":[]}";
  }
  else if(strcasecmp(\$q,"create$table") == 0){
	if($required){
		echo \$obj->add$uctable($parameters);
	}else{
		echo "{\\"results\\":[]}";
	}
  }
  else if(strcasecmp(\$q,"get$table") == 0){
	if(\$$key){
		echo \$obj->get$uctable(\$$key);
	}else{
		echo "{\\"results\\":[]}";
	}
  }
EDIT
;
}

sub genAPIdoc
{
# ## Create User
# http://rbi.appmatrix.mobi/api/rbi_api.php?q=createuser&firstname=testers&lastname=tester&type=0&email=test@appmatrixinc.com&password=12345
# `{"results":[{"success":"user created","user_id":"2","user_first":"testers","user_last":"tester"}]}`
  my $table = shift;
  my $uctable = ucfirst($table);
  shift;
  my @fields = @_;
  my ($field, $parameters, @parameters, @res, $results);
  # "SELECT ... FROM $table WHERE $key='".$data[$key]."'";
  
  $key = $fields[0];
  foreach $field (@fields)
  {
    if ($field ne $key)
    {	#push @updates, "$field = '\$$field'";
    
      push @parameters, "$field=$field";
      push @res, "\"$field\":\"$field\"";
    }
  }
  $parameters = join ('&', @parameters);
  $results = join (',', @res);
  $fields = join (', ', @fields);
  
  print <<EDIT
## Create $uctable
http://rbi.appmatrix.mobi/api/rbi_api.php?q=create$table&$parameters
`{"results":[{"success":"$table created","$table\_id":"2",$results}]}`

## Edit $uctable
http://rbi.appmatrix.mobi/api/rbi_api.php?q=edit$table&$key=1&$parameters
`{"results":[{"success":"$table updated","$table_id":"2",$results}]}`

## Get $uctable
http://rbi.appmatrix.mobi/api/rbi_api.php?q=get$table&$key=1
`{"results":[{"success":"found","$table_id":"2",$results}]}`

EDIT
;
}

while ($line = <>)
{
  if ($line =~ /^table\s+(\w+)/)
  {
    $oldtable = $table;
    $table = $1;
    if ($oldtable)
    {
      &genedit2($oldtable, @fields);
      &genadd($oldtable, @fields);
      &genget($oldtable, @fields);
      #&genAPIdoc($oldtable, @fields);
    }
    @fields = ();
  }
  elsif ($line =~ /^(\w+)/)
  {
    push @fields, $1;
  }
}

&genedit2($table, @fields);
&genadd($table, @fields);
&genget($table, @fields);
#&genAPIdoc($table, @fields);

