#!/usr/bin/perl

while ($line = <>)
{
  if ($line =~ /^table\s+(\w+)/)
  {
  }
  elsif ($line =~ /^(\w+)/)
  {
    $fields{$1} = 1;
  }
}

foreach $field (keys %fields)
{
  print "\$$field = preg_replace(\"/[&'<>%#;{}]/\",'',\$_REQUEST[\"$field\"]);\n";
}

